<?php

// Most of the codes here adapted from zen theme by John Albin
// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('zenland_rebuild_registry')) {
  drupal_rebuild_theme_registry();
}

/*
 * 
 * Dynamically toggles sidebar widths to certain .css via admin/build/themes/settings/zenland.
 */
if ($GLOBALS['theme'] == 'zenland') { // If we're in the main theme
  if (theme_get_setting('zenland_graphics') !== 'zenland-nographics') {
    drupal_add_css(drupal_get_path('theme', 'zenland') .'/css/header-graphics.css', 'theme', 'all');
  }
  if (theme_get_setting('zenland_sidewidth') == 'my-zenland-sidewidth') {
  	drupal_add_css(drupal_get_path('theme', 'zenland') .'/css/my-sidewidth.css', 'theme', 'all');
  }
}

/**
 * Implements HOOK_theme().
 */
function zenland_theme(&$existing, $type, $theme, $path) {
  if (!db_is_active()) {
    return array();
  }
  include_once './' . drupal_get_path('theme', 'zenland') . '/includes/template.theme-registry.inc';
  return _zenland_theme($existing, $type, $theme, $path);
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function zenland_breadcrumb($breadcrumb) {
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('zenland_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('zenland_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('zenland_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('zenland_breadcrumb_title')) {
        $trailing_separator = $breadcrumb_separator;
        $title = menu_get_active_title();
      }
      elseif (theme_get_setting('zenland_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }
      return '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . "$trailing_separator$title</div>";
    }
  }
  // Otherwise, return an empty string.
  return '';
}


/**
 * Override or insert PHPTemplate variables into the templates.
 */
function zenland_preprocess_page(&$vars, $hook) {
  
  // Add conditional stylesheets.
  if (!module_exists('conditional_styles')) {
    $vars['styles'] .= $vars['conditional_styles'] = variable_get('conditional_styles_' . $GLOBALS['theme'], '');
  }

  $vars['tabs2'] = menu_secondary_local_tasks();
  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $classes = split(' ', $vars['body_classes']);
  // Remove the mostly useless page-ARG0 class.
  if ($index = array_search(preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. drupal_strtolower(arg(0))), $classes)) {
    unset($classes[$index]);
  }
  if (!$vars['is_front']) {
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    $classes[] = zenland_id_safe('page-' . $path);
    // Add unique class for each website section.
    list($section, ) = explode('/', $path, 2);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        $section = 'node-add';
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        $section = 'node-' . arg(2);
      }
    }
    $classes[] = zenland_id_safe('section-' . $section);
  }


   if (theme_get_setting('zenland_layout') == 'layout-fixed') {
	 $classes[] = 'fixed';
   }
   else {
  	 $classes[] = 'liquid';
   }
  
   if ($vars['sidebar_top']  != '' || $vars['sidebar_bottom'] != '') { // add wide sidebar class
    $classes[] = 'wide-sidebar';
  }

  //Handle sidebar width alternatives, generated dynamically in admin/build/themes/settings/zenland
  switch(theme_get_setting('zenland_sidewidth')) {
    case 'zenland-210x210':
      $classes[] = 'zenland-210x210';
      break;
    case 'my-sidewidth':
      $classes[] = 'my-sidewidth';
      break;
	default:
	  $classes[] = '';//do not output any class to get defaults
  }
  
  
  switch(theme_get_setting('zenland_graphics')) {
    case 'zenland-myheader':
      $classes[] = 'myheader';
      break;
    case 'zenland-curl':
      $classes[] = 'curl';
      break;
    case 'zenland-cloud':
      $classes[] = 'cloud';
      break;
    case 'zenland-waves':
      $classes[] = 'waves';
    case 'zenland-header1':
	$classes[] = 'ornament-1';
      break;
	case 'zenland-header2':
	$classes[] = 'ornament-2';
      break;
	default:
	  $classes[] = '';//do not output any class to get defaults
  }


  $vars['body_classes_array'] = $classes;
  $vars['body_classes'] = implode(' ', $classes); // Concatenate with spaces.

}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function zenland_preprocess_node(&$vars, $hook) {
      // Special classes for nodes
  $classes = array('node');
  if ($vars['sticky']) {
    $classes[] = 'sticky';
  }
  if (!$vars['status']) {
    $classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['uid'] && $vars['uid'] == $GLOBALS['user']->uid) {
    $classes[] = 'node-mine'; // Node is authored by current user.
  }
  if ($vars['teaser']) {
    $classes[] = 'node-teaser'; // Node is displayed as teaser.
  }
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $classes[] = zenland_id_safe('node-type-' . $vars['type']);
  $vars['classes'] = implode(' ', $classes); // Concatenate with spaces
  
  
  //This will add quick admin links such as edit and Delete along node display
    //Original code from http://11heaven.com
    // If we have administer nodes permission
  if (user_access('administer nodes')) {
    // get the human-readable name for the content type of the node
    //$content_type_name = node_get_types('name', $vars['node']);
    // making a back-up of the old node links...
    $links = $vars['node']->links;
    $nid = $vars['node']->nid;
    $links['quick-edit'] = array(
      'title' => 'Edit',
      'href' => 'node/' . $nid . '/edit',
      'query' => drupal_get_destination(),
    );
    // and then adding the quick delete link
    $links['quick-delete'] = array(
      'title' => 'Delete',
      'href' => 'node/' . $nid . '/delete',
      'query' => drupal_get_destination(),
    );
	$vars['links'] = theme('links', $links, array('class' => 'links inline'));
  }
}

/**
 * Thanks to Zen theme
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function zenland_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  // Special classes for blocks.
  $classes = array('block');
  $classes[] = 'block-' . $block->module;
  $classes[] = 'region-' . $vars['block_zebra'];
  $classes[] = $vars['zebra'];
  $classes[] = 'region-count-' . $vars['block_id'];
  $classes[] = 'count-' . $vars['id'];

  $vars['edit_links_array'] = array();
  $vars['edit_links'] = '';
  if (theme_get_setting('zenland_block_editing') && user_access('administer blocks')) {
    include_once './' . drupal_get_path('theme', 'zenland') . '/includes/template.block-editing.inc';
    zenland_preprocess_block_editing($vars, $hook);
    $classes[] = 'with-block-editing';
  }

  // Render block classes.
  $vars['classes'] = implode(' ', $classes);
}

/**
 * Implements theme_menu_item_link()
 */
function zenland_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>';
    $link['localized_options']['html'] = TRUE;
  }

  return l($link['title'], $link['href'], $link['localized_options']);
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function zenland_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= '<ul class="tabs primary clear-block">' . $primary . '</ul>';
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= '<ul class="tabs secondary clear-block">' . $secondary . '</ul>';
  }

  return $output;
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
function zenland_preprocess_comment(&$vars, $hook) {
  include_once './' . drupal_get_path('theme', 'zenland') . '/includes/template.comment.inc';
  _zenland_preprocess_comment($vars, $hook);
}



/**
 * Allow themable wrapping of all comments.
 */
function zenland_comment_wrapper($content, $node) {

  /* If you need to customize some title, such as to Review This or Discuss This,
  otherwise use String overrides section in settings.php or stringoverride.module*/
  $content = str_replace(t('Post new comment'), t('<span>Post new comment</span>'), $content);
  $content = str_replace('' . t('Comment') . ':', t('<span>Comment</span>') . ':', $content);
  
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments"><span>'. t('Comments') .'</span></h2>'. $content .'</div>';
  }
}



function zenland_comment_submitted($comment) {
$user;
$vars['account'] = user_load(array('uid' => $comment->uid));

  return t('!userpic <br /> <strong>!username</strong> <br /> !datetime <br /> !daterel',
    array(
      '!username' => theme('username', $comment),
	  '!userpic' 	=> theme('user_picture', $vars['account']),
      '!datetime' => format_date($comment->timestamp, 'custom', 'F j, Y'),
      '!daterel' =>  format_interval(time() - $comment->timestamp),
    ));
}

function zenland_node_submitted($node) {
  return t('<strong>!username</strong> | !datetime - !daterel',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created, 'custom', 'F j, Y'),
      '!daterel' =>  format_interval(time() - $node->created),
    ));
}

/**
  * Theme override for theme_links()
  * Add active and span to menu item for easier theming such as tabs or icons
*/

function zenland_links($links, $attributes = array('class' => 'links')) {
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && $link['href'] == $_GET['q']) {
        $class .= ' active';
      }
      $output .= '<li class="'. $class .'">';

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $link['html'] = TRUE;
        $output .= l('<span>'. $link['title'] .'</span>', $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
  * Theme override for theme_menu_item()
  * Add menu ID and custom Name based on menu titles for block menus, great for icons
  * If necessary this may replace primary links with block primary menu for advanced theming
*/
function zenland_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }

  // Add unique identifier
  static $item_id = 0;
  $item_id += 1;
  $id .= 'mid-' . $item_id;
  // Add semi-unique class based on menu item name
  $class_added = zenland_id_safe(str_replace(' ', '_', strip_tags($link)));

  return '<li class="'. $class . ' ' . $class_added . '" id="' . $id . '">'. $link . $menu ."</li>\n";
}

/**
  * If you have trouble to create a custom maintenance page, see http://drupal.org/node/195435
*/
function zenland_maintenance_page($content, $messages = TRUE, $partial = FALSE) {
require_once 'maintenance-page.tpl.php';
}

/**
 * Set default form file input size 
 */
function zenland_file($element) {
  $element['#size'] = 40;
  return theme_file($element);
}



/**
 * Function spanify firstword
 * from acquia_slate theme
 */
function wordlimit($string, $length = 50, $ellipsis = "...") {
  $words = explode(' ', strip_tags($string));
  if (count($words) > $length)
    return implode(' ', array_slice($words, 0, $length)) . $ellipsis;
  else
    return $string;
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'id'.
 * - Replaces any character except alphanumeric characters with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *   The string
 * @return
 *   The converted string
 */
function zenland_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
  // If the first character is not a-z, add 'id' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}

/**
  * Customize drupal rss image here and also the title text
  * You replace the image as you please, adjust the class accordingly
  * If you want to change the icon position, change it in your page.tpl.php
  * Icon by http://www.icojoy.com/articles/23/
*/

function zenland_feed_icon($url, $title = NULL) {
  $title = t('Stay updated on the news');
  if ($image = theme('image', path_to_theme() . '/images/rss.png', t('Stay updated with the news'), $title)) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
  }
}

function zenland_more_link($url, $title) {
  return '<div class="more-link">['. t('<a href="@link" title="@title">see all</a>', array('@link' => check_url($url), '@title' => $title)) .']</div>';
}


/**
* Enable this if IMAGECACHE.module is installed. Customize to suit your need
* You must give the exact preset (says, 40x40px) and the default name is "avatar", other wise change it here
*/
function zenland_user_picture($account, $size = 'avatar') {
global $user;
$uid = arg(1);
$account = user_load(array('uid' =>$account->uid));
$naminasaha=  $account->name;

  if (variable_get('user_pictures', 0)) {
    // Display the user's photo if available
    if ($account->picture && file_exists($account->picture)) {
	$fotone = theme('imagecache', $size, $account->picture);
	$picture =  l($fotone, 'user/'. $account->uid ,
        array(
          'attributes' => array(
			'title' => $naminasaha
          ),
          'html' => TRUE
        )
      );

	} else {
	$fotono = theme('image', path_to_theme() . '/images/noavatar.png');
	$picture =  l($fotono, 'user/'. $account->uid ,
        array(
          'attributes' => array(
			'title' => $naminasaha . t('\'s No Avatar')
          ),
          'html' => TRUE
        )
      );
    }
    return '<div class="picture">'.$picture.'</div>';
  }
}


