<?php

$info = array(

  // Pre-defined color schemes.
  // A few are adapted from kuler community
  'schemes' => array(
    '#0072b9,#027ac6,#2385c2,#5ab5ee,#000000' => t('Blue Lagoon (Default)'),
    '#100d04,#5a561c,#e09b06,#d8ed96,#000000' => t('Africa'),
    '#464849,#223463,#2a2b2d,#7c9cab,#000000' => t('Ash'),
    '#55c0e2,#1d2767,#085360,#007e94,#000000' => t('Aquamarine'),
    '#971702,#a04000,#331900,#971702,#000000' => t('Belgian Chocolate'),
    '#3f3f3f,#336699,#6598cb,#6598cb,#000000' => t('Bluemarine'),
    '#26190d,#a04000,#b7691f,#fac652,#000000' => t('Bread'),
    '#a05000,#a04000,#a05000,#ffe8b0,#000000' => t('Caramel'),
    '#d0cb9a,#917803,#efde01,#e6fb2d,#000000' => t('Citrus Blast'),
    '#0f005c,#209ed9,#4d91ff,#1a1575,#000000' => t('Cold Day'),
    '#000000,#0f488a,#000000,#0a1f29,#000000' => t('Dark Night'),
    '#000000,#1a1a70,#000000,#031468,#000000' => t('Deep Blue'),
    '#2e110a,#8C321E,#8C321E,#D2B482,#000000' => t('Delicious'),
    '#10250e,#0c3c07,#03961e,#7be000,#000000' => t('Greenbeam'),
    '#777777,#000000,#555555,#c9c5c5,#000000' => t('Grayscale'),
    '#36362C,#5D917D,#36362C,#A8AD80,#000000' => t('Kayak'),
    '#2c2826,#84c23d,#f3f220,#a9dd36,#000000' => t('Lemon'),
    '#989098,#a04000,#ffc840,#a00040,#000000' => t('Maasai'),
    '#ffe23d,#a9290a,#fc6d1d,#a30f42,#000000' => t('Mediterrano'),
    '#788597,#3f728d,#a9adbc,#d4d4d4,#000000' => t('Mercury'),
    '#3568cc,#0040c0,#c0d0f0,#3568cc,#000000' => t('Microsoft'),
    '#5b5fa9,#5b5faa,#0a2352,#9fa8d5,#000000' => t('Nocturnal'),
    '#E3F23E,#6C821C,#6C821C,#E0E0AC,#000000' => t('New Planet'),
    '#7db323,#6a9915,#7db323,#b5d52a,#000000' => t('Olivia'),
    '#fcabfc,#f65572,#f2b1b1,#fcabfc,#000000' => t('Pink Pale'),
    '#12020b,#b00060,#f391c6,#f41063,#000000' => t('Pink Plastic'),
    '#272516,#7a7948,#7a7948,#B0A866,#000000' => t('Miserable Sadness'),
    '#b7a0ba,#c70000,#a1443a,#f21107,#000000' => t('Shiny Tomato'),
    '#D2B482,#191210,#c77223,#D2B482,#000000' => t('Tea Time'),
    '#52bf90,#008040,#34775a,#52bf90,#000000' => t('Teal Top'),
    '#bfef34,#78b319,#e0a000,#bfef34,#000000' => t('Withering Leaves'),
    '#8d5107,#52310f,#a37c43,#8d5107,#000000' => t('Wood'),
  ),

  // Images to copy over.
  'copy' => array(
    'images/sprite.png',
    'logo.png',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/style.css',
  ),

  // Coordinates of gradient (x, y, width, height).
  'gradient' => array(0, 0, 760, 180),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 760, 590),
    'link' => array(107, 533, 41, 23),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/body.png'                      => array(0, 0, 1,488),
    'images/bg-bar.png'                    => array(202, 530, 76, 14),
    'images/bg-bar-white.png'              => array(202, 506, 76, 14),
    'images/bg-tab.png'                    => array(107, 556, 41, 23),
    'images/bg-content-left.png'           => array(49, 148, 40, 352),
    'images/bg-content-right.png'          => array(511, 148, 40, 352),
    'images/bg-content.png'                => array(299, 148, 7, 200),
    'images/gradient-inner.png'            => array(646, 326, 110, 42),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  // Base file for image generation.
  'base_image' => 'color/base.png',
);
