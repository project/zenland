$(document).ready( function() {
  // Hide the breadcrumb details, if no breadcrumb.
  $('#edit-zenland-breadcrumb').change(
    function() {
      div = $('#div-zenland-breadcrumb-collapse');
      if ($('#edit-zenland-breadcrumb').val() == 'no') {
        div.slideUp('slow');
      } else if (div.css('display') == 'none') {
        div.slideDown('slow');
      }
    }
  );
  if ($('#edit-zenland-breadcrumb').val() == 'no') {
    $('#div-zenland-breadcrumb-collapse').css('display', 'none');
  }
  $('#edit-zenland-breadcrumb-title').change(
    function() {
      checkbox = $('#edit-zenland-breadcrumb-trailing');
      if ($('#edit-zenland-breadcrumb-title').attr('checked')) {
        checkbox.attr('disabled', 'disabled');
      } else {
        checkbox.removeAttr('disabled');
      }
    }
  );
  $('#edit-zenland-breadcrumb-title').change();
} );
