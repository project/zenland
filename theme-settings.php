<?php

// Most of theme-settings owe to johnalbin, Zen
// Include the definition of zenland_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'zenland') . '/includes/template.theme-registry.inc';


/**
 * Implementation of THEMEHOOK_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @param $subtheme_defaults
 *   Allow a subtheme to override the default values.
 * @return
 *   A form array.
 */
function zenland_settings($saved_settings, $subtheme_defaults = array()) {

  // Add the form's CSS
  drupal_add_css(drupal_get_path('theme', 'zenland') . '/css/theme-settings.css', 'theme');

  // Add javascript to show/hide optional settings
  drupal_add_js(drupal_get_path('theme', 'zenland') . '/js/theme-settings.js', 'theme');

  // Get the default values from the .info file.
  $defaults = zenland_theme_get_default_settings('zenland');

  // Allow a subtheme to override the default values.
  $defaults = array_merge($defaults, $subtheme_defaults);

  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);


  /*
   * Create the form using Forms API
   */
  $form['zenland-div-opening'] = array(
    '#value'         => '<div id="zenland-settings">',
  );
  

  $img_path = base_path() . drupal_get_path('theme', 'zenland') . "/header_images";
  $form['zenland_graphics'] = array(
    '#type'          => 'radios',
    '#title'         => t('Graphic alternatives'),
    '#options'       => array(
      'zenland-header1' => t('Ornament 01 <br /><small>(tilings)</small><br /><img src="' . $img_path . '/ornament-1.png">'),
      'zenland-header2' => t('Ornament 02 <br /><small>(tilings)</small><br /><img src="' . $img_path . '/ornament-2.png">'),
      'zenland-cloud' => t('Cloud <br /><small>(right positioned)</small><br /><img src="' . $img_path . '/cloud.png">'),
      'zenland-curl' => t('Curl <br /><small>(right positioned)</small><br /><img src="' . $img_path . '/curl.png">'),
      'zenland-waves' => t('Waves <br /><small>(center 960px)</small><br /><img src="' . $img_path . '/waves.png">'),
      'zenland-myheader' => t('My Header<br /><small>use your custom graphic (edit header-graphics.css to add one).</small>'),
	  'zenland-nographics' => t('None'),
                        ),
    '#default_value' => $settings['zenland_graphics'],
    '#description'   => t('<h4><strong>Switch to the working theme to preview header images.</strong></h4><br />Load associated CSS images from header-graphics.css. <br />Got cool graphics? Share your own and <a href="!link">let me know</a> to include yours in the next release.', array('!link' => 'http://gausarts.com/project/zenland')),
	'#attributes' => array('class' => 'header-graphics'),
  );


   $form['zenland_layout'] = array(
    '#type'          => 'radios',
    '#title'         => t('Layout method'),
    '#options'       => array(
                          'layout-liquid' => t('Liquid layout'),
                          'layout-fixed' => t('Fixed layout'),
                        ),
    '#default_value' => $settings['zenland_layout'],
  );
  
   $form['zenland_sidewidth'] = array(
    '#type'          => 'radios',
    '#title'         => t('Sidebar width alternatives'),
    '#options'       => array(
                          'zenland-sidewidth' => t('Default 125x300'),
                          'zenland-210x210' => t('Sidebar 210x210'),
                          'my-sidewidth' => t('Load my custom widths <small>(my-sidewidth.css)</small>'),
                          //'zenland_170x170' => t('Fixed 170x170'),
                        ),
    '#default_value' => $settings['zenland_sidewidth'],
    '#description'   => t('<strong>NOTE:</strong> If you want to override the widths, you <strong>MUST</strong> rename my-sidewidth-rename.css to my-sidewidth.css. If empty, it falls back to default.'),
  );

  $form['zenland_block_editing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show block editing on hover'),
    '#description'   => t('When hovering over a block, privileged users will see block editing links.'),
    '#default_value' => $settings['zenland_block_editing'],
  );

  $form['breadcrumb'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Breadcrumb settings'),
    '#attributes'    => array('id' => 'zenland-breadcrumb'),
  );
  $form['breadcrumb']['zenland_breadcrumb'] = array(
    '#type'          => 'select',
    '#title'         => t('Display breadcrumb'),
    '#default_value' => $settings['zenland_breadcrumb'],
    '#options'       => array(
                          'yes'   => t('Yes'),
                          'admin' => t('Only in admin section'),
                          'no'    => t('No'),
                        ),
  );
  $form['breadcrumb']['zenland_breadcrumb_separator'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Breadcrumb separator'),
    '#description'   => t('Text only. Don’t forget to include spaces.'),
    '#default_value' => $settings['zenland_breadcrumb_separator'],
    '#size'          => 5,
    '#maxlength'     => 10,
    '#prefix'        => '<div id="div-zenland-breadcrumb-collapse">', // jquery hook to show/hide optional widgets
  );
  $form['breadcrumb']['zenland_breadcrumb_home'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show home page link in breadcrumb'),
    '#default_value' => $settings['zenland_breadcrumb_home'],
  );
  $form['breadcrumb']['zenland_breadcrumb_trailing'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append a separator to the end of the breadcrumb'),
    '#default_value' => $settings['zenland_breadcrumb_trailing'],
    '#description'   => t('Useful when the breadcrumb is placed just before the title.'),
  );
  $form['breadcrumb']['zenland_breadcrumb_title'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Append the content title to the end of the breadcrumb'),
    '#default_value' => $settings['zenland_breadcrumb_title'],
    '#description'   => t('Useful when the breadcrumb is not placed just before the title.'),
    '#suffix'        => '</div>', // #div-zenland-breadcrumb
  );

  $form['themedev'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Theme development settings'),
    '#attributes'    => array('id' => 'zenland-themedev'),
  );
  
 $form['themedev']['zenland_rebuild_registry'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Rebuild theme registry on every page.'),
    '#default_value' => $settings['zenland_rebuild_registry'],
    '#description'   => t('During theme development, it can be very useful to continuously <a href="!link">rebuild the theme registry</a>. WARNING: this is a huge performance penalty and must be turned off on production websites.', array('!link' => 'http://drupal.org/node/173880#theme-registry')),
    '#prefix'        => '<div id="div-zenland-registry"><strong>' . t('Theme registry:') . '</strong>',
    '#suffix'        => '</div>',
  );

 $form['zenland-div-closing'] = array(
    '#value'         => '</div>',
  );

  // Return the form
  return $form;
}
