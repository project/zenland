<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
</head>

<body class="<?php print $body_classes; ?>">

<!-- Layout -->
<div id="wrapper">
   <div id="wrapper-inner">
    
    <div id="header-region" class="clear-block">
    <?php print $header; ?>
    <a name="navigation" id="navigation"></a>
    <div class="feed-icons"><?php print $feed_icons; ?></div>
    
	<?php if (isset($primary_links)) : ?>
    <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
    <?php endif; ?>

    </div><!-- /header-region -->

   <div id="container" class="clear-block">

      <div id="header">
        <div id="logo-floater">
        <?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          if ($site_slogan) {
            $site_fields[] = check_plain($site_slogan);
          }
          $site_title = implode(' ', $site_fields);
          if ($site_fields) {
            $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          }
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">';
            if ($logo) {
              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print $site_html .'</a></h1>';
          }
        ?>
        </div>
 
      </div> <!-- /header -->


      <div id="center"><div id="squeeze"><div class="right-corner"><div class="left-corner">

      <?php if (isset($secondary_links)) : ?>
       <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
      <?php endif; ?>
		
		<?php if ($breadcrumb || $mission || $title || $tabs || $help || $messages): ?>
		  <div id="content-header">
          <?php print $breadcrumb; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($title):print '<h1 class="title">'. $title .'</h1>'; endif;?>
          <?php if ($tabs): print '<div id="tabs-wrapper">'. $tabs. '</div>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
		  </div> <!-- /#content-header -->
        <?php endif; ?>
          <div class="clear-block">
          <?php if ($content_top): print '<div id="content-top" class="clear-block">'. $content_top .'</div>'; endif; ?>
		  <div id="content-area"><?php print $content ?></div>
          <?php if ($content_bottom): print '<div id="content-bottom" class="clear-block">'. $content_bottom .'</div>'; endif; ?>
          </div>


    <?php if ($footer || $footer_message): ?>
      <div id="footer"><div id="footer-inner" class="region-footer">

       <div id="footer-left">
      <?php if ($primary_links || $secondary_links): ?>
        <div id="back-to-top"><a href="#navigation"><?php print t('Top'); ?></a></div>
        <?php endif; ?>
           <?php print $footer; ?>
        </div>
        
        <?php if ($footer_message): ?>
          <div id="footer-message"><?php print $footer_message; ?></div>
        <?php endif; ?>

      </div></div> <!-- /#footer-inner, /#footer -->
    <?php endif; ?>
          

      </div></div></div></div> <!-- /.left-corner, /.right-corner, /#squeeze, /#center -->

      <?php if ($sidebar_top || $left || $right || $sidebar_bottom): ?>
      <div id="sidebar-wrapper"><div id="sidebar-wrapper-inner" class="region-sidebars"> 
	  
      <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>

      <?php if ($sidebar_top): ?>
        <div id="sidebar-top"><div id="sidebar-top-inner" class="sidebar-wide region-top">
          <?php print $sidebar_top ?>
        </div></div>
      <?php endif; ?>

      <?php if ($left): ?>
        <div id="sidebar-left"><div id="sidebar-left-inner" class="sidebar region-left">
          <?php print $left ?>
        </div></div>
      <?php endif; ?>
      

      <?php if ($right): ?>
        <div id="sidebar-right"><div id="sidebar-right-inner" class="sidebar region-right">
          <?php print $right ?>
        </div></div>
      <?php endif; ?>
      
      <?php if ($sidebar_bottom): ?>
        <div id="sidebar-bottom"><div id="sidebar-bottom-inner" class="sidebar-wide region-bottom">
          <?php print $sidebar_bottom ?>
        </div></div>
      <?php endif; ?>

     </div></div> <!-- /sidebar-wrapper-inner, /sidebar-wrapper -->
      <?php endif; ?>

    </div> <!-- /container -->
  </div>
  
</div>
<!-- /layout -->
  <?php print $scripts ?>
  <?php print $closure ?>
  </body>
</html>
